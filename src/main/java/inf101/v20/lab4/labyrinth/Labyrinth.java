package inf101.v20.lab4.labyrinth;

import java.awt.Color;

import inf101.v20.lab4.grid.IGrid;

public class Labyrinth implements ILabyrinth {
	public static void checkState(Labyrinth obj) {
		boolean ok = !obj.playerSet || obj.isValidPos(obj.playerX, obj.playerY);
		int numPlayers = 0;
		for (int y = 0; y < obj.tiles.getHeight(); y++) {
			for (int x = 0; x < obj.tiles.getWidth(); x++) {
				if (obj.tiles.get(x, y) == LabyrinthTile.PLAYER) {
					numPlayers++;
				}
			}
		}
		if (obj.playerSet) {
			ok &= numPlayers == 1;
		} else {
			ok &= numPlayers == 0;
		}
		if (!ok)
			throw new IllegalStateException("bad object");
	}

	private IGrid<LabyrinthTile> tiles;
	private int playerX = -1;
	private int playerY = -1;

	boolean playerSet = false;

	public Labyrinth(IGrid<LabyrinthTile> tiles) throws LabyrinthParseException {
		if(tiles == null)
			throw new IllegalArgumentException();
		
		this.tiles = tiles;

		int numPlayers = 0;
		for (int y = 0; y < tiles.getHeight(); y++) {
			for (int x = 0; x < tiles.getWidth(); x++) {
				if (tiles.get(x, y) == LabyrinthTile.PLAYER) {
					numPlayers++;
					playerX = x;
					playerY = y;
					playerSet = true;
				}
			}
		}
		if (numPlayers != 1)
			throw new LabyrinthParseException("Labyrinth created with " + numPlayers + " number of players!");

		checkState(this);
	}

	@Override
	public LabyrinthTile getCell(int x, int y) {
		if (!isValidPos(x, y))
			throw new IllegalArgumentException("x,y invalid");

		return tiles.get(x, y);
	}

	@Override
	public Color getColor(int x, int y) {
		if(!isValidPos(x, y))
			throw new IllegalArgumentException("x,y invalid");
		
		return tiles.get(x, y).getColor();
	}

	@Override
	public int getHeight() {
		return tiles.getHeight();
	}

	@Override
	public int getPlayerGold() {
		return 0;
	}

	@Override
	public int getPlayerHitPoints() {
		return 0;
	}

	@Override
	public int getWidth() {
		return tiles.getWidth();
	}

	@Override
	public boolean isPlaying() {
		return playerSet;
	}

	private boolean isValidPos(int x, int y) {
		return x >= 0 && x < tiles.getWidth() //
				&& y >= 0 && y < tiles.getHeight();
	}

	@Override
	public void movePlayer(Direction d) throws MovePlayerException {
		if (!playerCanGo(d))
			throw new MovePlayerException("Player cannot go in the chosen direction!");

		switch (d) {
		case EAST:
			tiles.set(playerX, playerY, LabyrinthTile.OPEN);
			tiles.set(playerX + 1, playerY, LabyrinthTile.PLAYER);
			playerX++;
			break;
		case NORTH:
			tiles.set(playerX, playerY, LabyrinthTile.OPEN);
			tiles.set(playerX, playerY + 1, LabyrinthTile.PLAYER);
			playerY++;
			break;
		case SOUTH:
			tiles.set(playerX, playerY, LabyrinthTile.OPEN);
			tiles.set(playerX, playerY - 1, LabyrinthTile.PLAYER);
			playerY--;
			break;
		case WEST:
			tiles.set(playerX, playerY, LabyrinthTile.OPEN);
			tiles.set(playerX - 1, playerY, LabyrinthTile.PLAYER);
			playerX--;
			break;
		default:
			assert false;
		}

		checkState(this);
	}

	@Override
	public boolean playerCanGo(Direction d) {
		if(d == null)
			throw new IllegalArgumentException();
		
		int newX = playerX, newY = playerY;

		switch (d) {
		case EAST:
			newX += 1;
			break;
		case NORTH:
			newY += 1;
			break;
		case SOUTH:
			newY -= 1;
			break;
		case WEST:
			newX -= 1;
			break;
		default:
			throw new IllegalStateException("This can't happen");
		}

		return playerCanGoTo(newX, newY);
	}

	public boolean playerCanGoTo(int x, int y) {
		if (!isValidPos(x, y))
			return false;
		
		if (tiles.get(x, y) == LabyrinthTile.WALL)
			return false;

		// ok
		return true;
	}

	@Override
	public String toString() {
		String toRet = "";

		for (int y = tiles.getHeight() - 1; y >= 0; y--) {
			for (int x = 0; x < tiles.getWidth(); x++) {
				switch (tiles.get(x, y)) {
				case OPEN:
					toRet += " ";
					break;
				case PLAYER:
					toRet += "P";
					break;
				case GOLD:
					toRet += "g";
					break;
				case MONSTER:
					toRet += "m";
					break;
				case WALL:
					toRet += "*";
					break;
				default:
					assert (false);
					break;
				}
			}
			toRet += "\n";
		}
		return toRet;
	}
}
