package inf101.v20.lab4.labyrinth;

public enum Direction {
	NORTH, SOUTH, EAST, WEST
}
