package inf101.v20.lab4.cellular;

public enum Direction {

	NORTH,
	EAST,
	SOUTH,
	WEST
}
