package inf101.v20.lab4.cellular;

import java.util.Random;

import inf101.v20.lab4.cellular.cellstate.CellState;
import inf101.v20.lab4.grid.Grid;
import inf101.v20.lab4.grid.IGrid;
/**
 * 
 * A Cell Automata that implements Conway's Game of Life.
 * 
 * Every cell has two states: Alive or Dead. 
 * Each cell (except along the border) has eight neighbors: diagonal, horizontal and lateral. 
 * 
 * On each step the cell state is updated according to the Game of Life rules: 
 * 
 * Any live cell with fewer than two live neighbors dies, as if by underpopulation.
 * Any live cell with two or three live neighbors lives on to the next generation.
 * Any live cell with more than three live neighbors dies, as if by overpopulation.
 * Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
 * 
 * @author eivind, Anna Eilertsen - anna.eilertsen@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells  
	 */
	IGrid<CellState> currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width The width of the grid of cells 
	 */
	public GameOfLife(int width, int height) {
		currentGeneration = new Grid<CellState>(width, height, CellState.DEAD);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int x = 0; x < currentGeneration.getWidth(); x++) {
			for (int y = 0; y < currentGeneration.getHeight(); y++) {
				if (random.nextBoolean()) {
					currentGeneration.set(x, y, CellState.ALIVE);
				} else {
					currentGeneration.set(x, y, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.getHeight();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.getWidth();
	}

	@Override
	public CellState getCellState(int x, int y) {
		return currentGeneration.get(x, y);
	}

	@Override
	public void step() {

		IGrid<CellState> nextGeneration = new Grid<CellState>(
				currentGeneration.getWidth(), currentGeneration.getHeight(),
				CellState.ALIVE);

		for (int x = 0; x < currentGeneration.getWidth(); x++) {
			for (int y = 0; y < currentGeneration.getHeight(); y++) {
				int numNeighbours = getLivingNeigbours(x, y);
				if (numNeighbours < 2) {
					nextGeneration.set(x, y, CellState.DEAD);
				} else if (numNeighbours == 3) {
					nextGeneration.set(x, y, CellState.ALIVE);
				} else if (numNeighbours > 3) {
					nextGeneration.set(x, y, CellState.DEAD);
				} else {
					nextGeneration.set(x, y, currentGeneration.get(x, y));
				}
			}
		}

		currentGeneration = nextGeneration;

	}

	private int getLivingNeigbours(int x, int y) {
		int numNeighbours = 0;
		for (int dx = -1; dx <= 1; dx++) {
			for (int dy = -1; dy <= 1; dy++) {
				if (dx == 0 && dy == 0)
					continue; // samme celle, hopp over
				if (y + dy < 0)
					continue; // utenfor brettet
				if (y + dy >= currentGeneration.getHeight())
					continue; // utenfor brettet
				if (x + dx < 0)
					continue; // utenfor brettet
				if (x + dx >= currentGeneration.getWidth())
					continue; // utenfor brettet
				
				// tell levende naboer
				if (currentGeneration.get(x + dx, y + dy) == CellState.ALIVE) {
					numNeighbours++;
				}
			}
		}
		return numNeighbours;
	}
}
