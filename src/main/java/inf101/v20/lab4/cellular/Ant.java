package inf101.v20.lab4.cellular;

public class Ant {
	
	protected int x, y;
	protected Direction dir;
	
	public Ant(int x, int y, Direction dir) {
		this.x = x;
		this.y = y;
		this.dir = dir;
	}
	
	public void turnLeft() {
		switch (dir) {
		case NORTH : {x--; dir = Direction.WEST; break;}
		case SOUTH : {x++; dir = Direction.EAST; break;}
		case EAST : {y--; dir = Direction.NORTH; break;}
		case WEST : {y++; dir = Direction.SOUTH; break;}
		}
	}

	
	public void turnRight() {
		switch (dir) {
		case NORTH : {x++; dir = Direction.EAST; break;}
		case SOUTH : {x--; dir = Direction.WEST; break;}
		case EAST : {y++; dir = Direction.SOUTH; break;}
		case WEST : {y--; dir = Direction.NORTH; break;}
		}
	}
	
	public int getX() { return x; }
	public void setX(int x) { this.x = x; }
	
	public int getY() { return y; }
	public void setY(int y) { this.y = y; }
	
	public Ant copy() {
		return new Ant(x, y, dir);
	}

}
