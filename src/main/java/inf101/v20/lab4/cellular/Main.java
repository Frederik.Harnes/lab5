package inf101.v20.lab4.cellular;

import inf101.v20.lab4.cellular.gui.CellAutomataGUI;

public class Main {

	public static void main(String[] args) {

		//CellAutomaton ca = new LangtonsAnt(100,100, "RRLLLRLLLRRR");
		//CellAutomaton ca = new SeedsAutomaton(100,100);
		CellAutomaton ca = new GameOfLife(100,100);
		
		CellAutomataGUI.run(ca);
	}

}
