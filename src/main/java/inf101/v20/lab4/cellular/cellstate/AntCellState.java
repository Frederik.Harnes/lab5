package inf101.v20.lab4.cellular.cellstate;

import java.awt.Color;

public class AntCellState implements ICellState {

	@Override
	public Color toColor() {
		return Color.yellow;
	}
	
	@Override
	public int getValue() {
		return -1;
	}
	
}
