package inf101.v20.lab4.cellular.cellstate;

import java.awt.Color;

public interface ICellState {
	
	/**
	 * The color that represents the the cell state
	 * @return The color that represents the the cell state
	 */
	public Color toColor();
	
	/**
	 * The state of the cell as a number
	 * @return The state of the cell as a number
	 */
	public int getValue();

}
