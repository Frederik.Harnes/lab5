package inf101.v20.lab4.cellular;


import inf101.v20.lab4.cellular.cellstate.AntCellState;
import inf101.v20.lab4.cellular.cellstate.CellState;
import inf101.v20.lab4.cellular.cellstate.ICellState;
import inf101.v20.lab4.grid.Grid;
import inf101.v20.lab4.grid.IGrid;

public class LangtonsAnt implements CellAutomaton {
	
	/**
	 * The maximum length of any valid rule. Should not exceed 256, 
	 * otherwise not all colors will be distinct.
	 */
	private static final int MAX_RULE_LENGTH = 32;
	
	/**
	 * Stores the rule, in the following sense: Upon reading a state whose value is i, 
	 * char[i] indicates whether to turn left ('L') or right ('R')
	 */
	private char[] rule;
	
	/**
	 * A grid of the cell states in the current generation
	 */
	private IGrid<ICellState> currentGeneration;
	
	/**
	 * The ant
	 */
	private Ant ant;
	
	/**
	 * The cell state representing an ant
	 */
	private final AntCellState antState = new AntCellState();
	
	/**
	 * The state the ant saw upon moving to the field it is placed on 
	 * currently. Determines the next move (using the given rule).
	 */
	protected ICellState seenState;
	
	public LangtonsAnt(int width, int height, String rule) {
		this.currentGeneration = new Grid<>(width, height, null);
		checkRule(rule);
		this.rule = rule.toCharArray();
	}
	
	/**
	 * Checks the rule for validity.
	 * A string is not a rule its length exceeds the maximum length ({@link #MAX_RULE_LENGTH}) 
	 * or if it contains characters other than 'L' and 'R'.
	 * 
	 * @param rule
	 */
	private void checkRule(String rule) {
		if (rule.length() > MAX_RULE_LENGTH) {
			throw new IllegalArgumentException("The rule " +rule+ " is too long. Its length is "
				+rule.length()+ " while MAX_RULE_LENGTH = " +MAX_RULE_LENGTH);
		}
		char[] ruleChars = rule.toCharArray();
		for (int i = 0; i < ruleChars.length; i++) {
			if (ruleChars[i] != 'R' && ruleChars[i] != 'L')
				throw new IllegalArgumentException("The rule " +rule+ 
					" is not a valid rule (see " +ruleChars[i]+ 
					" at position " +i+ ").");
		}
	}

	@Override
	public ICellState getCellState(int x, int y) {
		// This method returns different shades of the same color.
		// The scaling depends on the length of the given rule.
		return currentGeneration.get(x, y);
//		if (state == CellState.ANT) return Color.yellow;
//		int val = (int) (state.getValue()*256/rule.length);
//		return new Color(255, 255-val, 255-val);
	}

	@Override
	public void initializeCells() {
		// Set all fields to be in state 0.
		for (int x = 0; x < currentGeneration.getWidth(); x++) {
			for (int y = 0; y < currentGeneration.getHeight(); y++) {
				currentGeneration.set(x, y, new CellState(0, rule.length));
			}
		}
		// Initialize the seenState field (All fields are 0 in the beginning)
		this.seenState = new CellState(0, rule.length);
		// Place the ant and initialize the corresponding field variables.
		int midX = (int) (this.currentGeneration.getWidth() / 2),
		    midY = (int) (this.currentGeneration.getHeight() / 2);
		this.ant = new Ant(midX, midY, Direction.NORTH);
		this.currentGeneration.set(ant.getX(), ant.getY(), new AntCellState());
	}

	@Override
	public void step() {
		int color = seenState.getValue();
		Ant nextAnt = ant.copy();
		
		if (rule[color] == 'L') {
			// turn left
			nextAnt.turnLeft();
		}
		if (rule[color] == 'R') {
			// turn right
			nextAnt.turnRight();
		}
		// throw back into field if necessary
		if (nextAnt.getX() >= currentGeneration.getWidth()) nextAnt.setX(currentGeneration.getWidth() - 3);
		if (nextAnt.getY() >= currentGeneration.getHeight()) nextAnt.setY(currentGeneration.getHeight() - 3);
		if (nextAnt.getX() < 0) nextAnt.setX(2);
		if (nextAnt.getY() < 0) nextAnt.setY(2);
		
		// Update the state of the filed the ant is leaving.
		currentGeneration.set(ant.getX(), ant.getY(), new CellState((color + 1) % rule.length, rule.length));
		
		// Update the state the ant is reading in the field it is moving to.
		seenState = currentGeneration.get(nextAnt.getX(), nextAnt.getY());
		// Move the ant to the new position
		currentGeneration.set(nextAnt.getX(), nextAnt.getY(), new AntCellState());
		// Update the field variables corresponding to the ant's position.
		this.ant = nextAnt;
	}

	@Override
	public int numberOfRows() {
		return this.currentGeneration.getHeight();
	}

	@Override
	public int numberOfColumns() {
		return this.currentGeneration.getWidth();
	}

}
